export default defineEventHandler((event) => {
    return {
      data: {
          query: {
            source: "iDzcTHVRTj9Kufcf",
            suggest: null
          },
          groups: {
            tips: [
              {id: 7669, q: "MNnYixrR1tx6F9hS"},
              {id: 9996, q: "iPPV3ZyFz7Q1M6n5"},
              {id: 35429, q: "s4k2DM78FWzyzeHf"}
            ],
            documents: [
              {name: "FmmDOitdMBwBJ2qs", link: "https:\/\/docs.cntd.ru\/document\/1301469977"},
              {name: "5dKGdJ17rPneHcjT", link: "https:\/\/docs.cntd.ru\/document\/1301469977"},
              {name: "F7dYYBnCrDQP06dv", link: "https:\/\/docs.cntd.ru\/document\/1301469977"}
            ]
          }
      },
      meta: {
        version: "0.0.0-rc14",
        revision: "26e61485"
      }
    }
  })